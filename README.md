## Gorcery List App
A grocery list app using the MEAN.js stack.

## Time Tracking
Start Time: 8:35PM

End Time: 12:53am

## Runnning the App
To run the app we first:
`npm install`

To start the database service open terminal and run:
`mongod`

To start the web server open terminal and run:
`npm start`

## Thank You
Thanks for giving me the opportunity! I am looking forward to your feedback!

Trevor