'use strict';

module.exports = function(app) {
    var groceries = require('../../app/controllers/groceries.server.controller');
    var users = require('../../app/controllers/users.server.controller');

    app.route('/groceries')
        .get(users.requiresLogin, groceries.list)
        .post(users.requiresLogin, groceries.create);

    app.route('/groceries/:groceryId')
        .get(users.requiresLogin, groceries.read)
        .put(users.requiresLogin, groceries.hasAuthorization, groceries.update)
        .delete(users.requiresLogin, groceries.hasAuthorization, groceries.delete);

    app.param('groceryId', groceries.groceryByID);
};