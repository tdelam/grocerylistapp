'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * Grocery Schema
 */
var GrocerySchema = new Schema({
    // This field should be required
    item_name: {
        type: String,
        default: '',
        trim: true,
        unique: true,
        required: 'item name cannot be blank.',
    },
    quantity: {
        type: Number,
        default: 0,
        min: 0
    },
    created_at: {
        type: Date,
        default: Date.now
    },
    user: {
        type: Schema.ObjectId,
        ref: 'User',
    }
});

mongoose.model('Grocery', GrocerySchema);