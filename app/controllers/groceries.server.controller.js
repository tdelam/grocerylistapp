'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    errorHandler = require('./errors.server.controller'),
    Grocery = mongoose.model('Grocery'),
    _ = require('lodash');

/**
 * Create a Grocery
 */
exports.create = function(req, res) {
    var grocery_item = new Grocery(req.body);
    
    grocery_item.user = req.user;

    grocery_item.save(function(error) {
        if (error) { 
            return res.status(400).send({
                message: errorHandler.getErrorMessage(error)
            });
        } else {
            res.status(201).json(grocery_item);
        }
    });
};

/**
 * Show the current Grocery
 */
exports.read = function(req, res) {
    Grocery.findById(req.params.groceryId).populate('user', 'displayName').exec(function(error, grocery_item) {
        if(error) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(error)
            });
        } else {
            if(!grocery_item) {
                return res.status(400).send({
                    message: 'Grocery item not found.'
                });
            }
            res.json(grocery_item);
        }
    });
};

/**
 * Update a Grocery
 */
exports.update = function(req, res) {
    var grocery_item = req.grocery_item;

    grocery_item = _.assign(grocery_item, req.body);
    
    grocery_item.save(function(error) {
        if(error) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(error)
            });
        } else {
            res.json(grocery_item);
        }
    });
};

/**
 * Delete an Grocery
 */
exports.delete = function(req, res) {
    var grocery_item = req.grocery_item;

    grocery_item.remove(function(error) {
        if(error) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(error)
            });
        } else {
            res.json(grocery_item);
        }
    });
};

/**
 * List of Groceries
 */
exports.list = function(req, res) {
    Grocery.find({user: req.user._id})
        .sort('-created_at').populate('user', 'displayName').exec(function(error, groceries) {
        if(error) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(error)
            });
        } else {
            // filter in users grocery items
            var userGroceryItems = req.user ? groceries.filter(function(i) { 
                return i.user.id === req.user.id; 
            }) : [];
            res.json(userGroceryItems);
        }
    });
};

/**
 * Grocery middleware
 */
exports.groceryByID = function(req, res, next, id) {
 
    if (!mongoose.Types.ObjectId.isValid(id)) {
        return res.status(400).send({
            message: 'Grocery is invalid'
        });
    }
 
    Grocery.findById(id).populate('user', 'displayName').exec(function(error, grocery_item) {
        if (error) return next(error);
        if (!grocery_item) {
            return res.status(404).send({
                message: 'Grocery not found'
            });
        }
        req.grocery_item = grocery_item;
        next();
    });
};

/**
 * Grocery authorization middleware
 * I noticed as I was building this that we should
 * not allow the user from deleting/updating a list
 * that does not belong to them
 */
exports.hasAuthorization = function(req, res, next) {
    if (req.grocery_item.user.id !== req.user.id) {
        return res.status(403).send('User is not authorized');
    }
    next();
};