'use strict';

//Grocery service used to communicate Grcoeries REST endpoints
angular.module('groceries').factory('Groceries', ['$resource',
	function($resource) {
		return $resource('groceries/:groceryId', {
			groceryId: '@_id'
		},{
			update: {
				method: 'PUT',
			}
		});
	}
]);