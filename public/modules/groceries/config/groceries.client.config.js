'use strict';

// Groceries module config
angular.module('groceries').run(['Menus',
	function(Menus) {
        Menus.addMenuItem('topbar', 'Groceries', 'groceries', 'dropdown', '/groceries(/create)?');
        Menus.addSubMenuItem('topbar', 'groceries', 'Current Grocery List', 'groceries');
        Menus.addSubMenuItem('topbar', 'groceries', 'New Grocery Item', 'groceries/create');
	}
]);